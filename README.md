## 4 command assembly language interpreter

Implement an interpreter for an assembly language consisting of 4 commands:

```
mov a b
```

Copy the value of b into the register 'a'. 
'b' can be both an integer and another register.

```
inc a 
```

Increment the contents of register 'a' by 1.


```
dec a
```

Decrement the contents of register 'a' by 1.


```
jmp a b
```
Jump the execution pointer b steps away. 
Do it only if 'a' is not 0. 'a' can be both an integer or 
a reference to a register.

## Sample program:

```
mov a 5
inc a
dec a
dec a
jmp a -1
inc a
```


See the sample programs in the tests.py file.